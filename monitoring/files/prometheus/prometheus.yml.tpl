global:
  scrape_interval: 30s
  evaluation_interval: 30s

scrape_configs:
  - job_name: "grafana"
    scheme: https
    basic_auth:
      password: ${GRAFANA_METRICS_PASSWORD}
      username: ${GRAFANA_METRICS_USERNAME}
    tls_config:
      server_name: ${DOMAIN}
    static_configs:
      - targets:
        - "localhost:443"
  - job_name: "prometheus"
    static_configs:
      - targets:
        - "localhost:9090"
  - job_name: "push-gateway"
    honor_labels: true
    scheme: https
    basic_auth:
      password: ${PUSH_GATEWAY_PASSWORD}
      username: ${PUSH_GATEWAY_USERNAME}
    tls_config:
      server_name: ${DOMAIN}
    static_configs:
      - targets:
        - "localhost:9091"
  - job_name: "blackbox"
    scrape_interval: 10m
    static_configs:
      - targets:
        - "localhost:9115"
  - job_name: "blackbox-dns-doh"
    scrape_interval: 5m
    metrics_path: /probe
    params:
      module:
        - http_doh
    dns_sd_configs:
      - names:
        - ${DOMAIN_DNS}
        type: AAAA
        port: 443
    relabel_configs:
      - source_labels:
        - __address__
        replacement: "https://${1}/dns-query"
        target_label: __param_target
      - source_labels:
        - __address__
        target_label: instance
      - source_labels:
        - __meta_dns_name
        target_label: __param_hostname
      - replacement: "localhost:9115"
        target_label: __address__
  - job_name: "blackbox-dns-dot"
    scrape_interval: 5m
    metrics_path: /probe
    params:
      module:
        - tcp_tls
    dns_sd_configs:
      - names:
        - ${DOMAIN_DNS}
        type: AAAA
        port: 853
    relabel_configs:
      - source_labels:
        - __address__
        target_label: __param_target
      - source_labels:
        - __address__
        target_label: instance
      - source_labels:
        - __meta_dns_name
        target_label: __param_hostname
      - replacement: "localhost:9115"
        target_label: __address__
  - job_name: "blackbox-static"
    scrape_interval: 5m
    metrics_path: /probe
    static_configs:
      - targets:
        - "dsm-webdav/http_webdav/https://${DOMAIN_DSM}:${PORT_DSM_WEBDAV}"
        - "dsm-calendar/http_default/https://${DOMAIN_DSM}:${PORT_DSM_CALENDAR}"
        - "dsm-contacts/http_default/https://${DOMAIN_DSM}:${PORT_DSM_CONTACTS}"
        - "dsm-drive-http/http_default/https://${DOMAIN_DSM}:${PORT_DSM_DRIVE_HTTP}"
        - "dsm-drive-tcp/tcp_default/${DOMAIN_DSM}:${PORT_DSM_DRIVE_TCP}"
        - "location-fmd/http_default/https://${DOMAIN_LOCATION}/fmd/"
        - "location-traccar/http_default/https://${DOMAIN_LOCATION}/"
        - "monitoring/http_default/https://${DOMAIN}/healthz"
        - "streaming/tcp_tls/${DOMAIN_STREAMING}:${PORT_STREAMING_RTMPS}"
        - "website-arch/http_default/https://${DOMAIN_WEBSITE_ARCH}/bitservices/x86_64/bitservices.db"
        - "website-rich/http_default/https://${DOMAIN_WEBSITE_RICH}"
        - "website-www/http_default/https://${DOMAIN_WEBSITE_WWW}"
    relabel_configs:
      - source_labels:
        - __address__
        regex: '^([^\/]+)\/[^\/]+\/.+$'
        target_label: instance
      - source_labels:
        - __address__
        regex: '^[^\/]+\/([^\/]+)\/.+$'
        target_label: __param_module
      - source_labels:
        - __address__
        regex: '^[^\/]+\/[^\/]+\/(.+)$'
        target_label: __param_target
      - replacement: "localhost:9115"
        target_label: __address__
  - job_name: "node"
    ec2_sd_configs:
      - port: 9100
        region: ${REGION}
        filters:
          - name: tag:Region
            values:
              - ${REGION}
          - name: tag:VPC
            values:
              - ${VPC}
    relabel_configs:
      - source_labels:
        - __meta_ec2_tag_Name
        target_label: instance_name
      - source_labels:
        - __meta_ec2_tag_Class
        target_label: instance_class
      - source_labels:
        - __meta_ec2_tag_Config
        target_label: instance_config
      - source_labels:
        - __meta_ec2_tag_Service
        target_label: instance_service
      - source_labels:
        - __meta_ec2_default_ipv6_address
        target_label: __address__
        action: replace
        replacement: "[$1]:9100"
  - job_name: "dns"
    ec2_sd_configs:
      - port: 9153
        region: ${REGION}
        filters:
          - name: tag:Config
            values:
              - svc-${VPC}-${REGION}
          - name: tag:Region
            values:
              - ${REGION}
          - name: tag:Service
            values:
              - dns
          - name: tag:VPC
            values:
              - ${VPC}
    relabel_configs:
      - source_labels:
        - __meta_ec2_tag_Name
        target_label: instance_name
      - source_labels:
        - __meta_ec2_tag_Class
        target_label: instance_class
      - source_labels:
        - __meta_ec2_default_ipv6_address
        target_label: __address__
        action: replace
        replacement: "[$1]:9153"
  - job_name: "proxy"
    scrape_interval: 1m # https://github.com/boynux/squid-exporter/issues/97
    scrape_timeout: 30s # https://github.com/boynux/squid-exporter/issues/97
    ec2_sd_configs:
      - port: 9301
        region: ${REGION}
        filters:
          - name: tag:Config
            values:
              - admin-${VPC}-${REGION}
          - name: tag:Region
            values:
              - ${REGION}
          - name: tag:Service
            values:
              - proxy
          - name: tag:VPC
            values:
              - ${VPC}
    relabel_configs:
      - source_labels:
        - __meta_ec2_tag_Name
        target_label: instance_name
      - source_labels:
        - __meta_ec2_tag_Class
        target_label: instance_class
