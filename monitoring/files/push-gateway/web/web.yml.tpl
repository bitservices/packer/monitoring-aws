tls_server_config:
  key_file: /monitoring/certs/key.pem
  cert_file: /monitoring/certs/certificate.crt
  min_version: "TLS13"

basic_auth_users:
  ${PUSH_GATEWAY_USERNAME}: "${PUSH_GATEWAY_BCRYPT}"
