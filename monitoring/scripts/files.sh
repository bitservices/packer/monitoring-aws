#!/bin/bash -e
#
# Sets permissions and installs files uploaded by Packer.
#
###############################################################################

set -o pipefail

###############################################################################

hash mv
hash rm
hash sudo
hash chmod
hash chown

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Setting file permissions...                                          -"
echo "------------------------------------------------------------------------"

sudo chown root:grafana "/tmp/files/grafana/grafana.ini"
sudo chmod 640 "/tmp/files/grafana/grafana.ini"

sudo chown -R root:root "/tmp/files/grafana/grafana.service.d"
sudo chmod -R u+rwX,go+rX,go-w "/tmp/files/grafana/grafana.service.d"

sudo chown -R grafana:grafana "/tmp/files/grafana/dashboards"
sudo chmod -R u+rX,u-w,go-rwx "/tmp/files/grafana/dashboards"
sudo chown -R grafana:grafana "/tmp/files/grafana/provisioning"
sudo chmod -R u+rX,u-w,go-rwx "/tmp/files/grafana/provisioning"

sudo chown -R root:root "/tmp/files/prometheus"
sudo chmod 644 "/tmp/files/prometheus/config"
sudo chmod 600 "/tmp/files/prometheus/prometheus.yml.env"
sudo chmod 600 "/tmp/files/prometheus/prometheus.yml.tpl"

sudo chown root:root "/tmp/files/push-gateway/config"
sudo chmod 644 "/tmp/files/push-gateway/config"

sudo chown -R push_gateway:push_gateway "/tmp/files/push-gateway/web"
sudo chmod -R u+rX,u-w,go-rwx "/tmp/files/push-gateway/web"

sudo chown -R root:root "/tmp/files/blackbox"
sudo chmod 644 "/tmp/files/blackbox/config"
sudo chmod 644 "/tmp/files/blackbox/blackbox.yml"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Installing files...                                                  -"
echo "------------------------------------------------------------------------"

sudo mv -fv "/tmp/files/grafana/grafana.ini" "/etc/grafana.ini"

sudo mv -fv "/tmp/files/grafana/grafana.service.d" "/etc/systemd/system/"

sudo mv -fv "/tmp/files/grafana/dashboards" "/monitoring/grafana/"
sudo mv -fv "/tmp/files/grafana/provisioning" "/monitoring/grafana/"

sudo mv -fv "/tmp/files/prometheus/config" "/etc/conf.d/prometheus"
sudo mv -fv "/tmp/files/prometheus/prometheus.yml.env" "/etc/prometheus/prometheus.yml.env"
sudo mv -fv "/tmp/files/prometheus/prometheus.yml.tpl" "/etc/prometheus/prometheus.yml.tpl"

sudo mv -fv "/tmp/files/push-gateway/config" "/etc/conf.d/prometheus-push-gateway"

sudo mv -fv "/tmp/files/push-gateway/web" "/monitoring/push-gateway/"

sudo mv -fv "/tmp/files/blackbox/config" "/etc/conf.d/prometheus-blackbox-exporter"
sudo mv -fv "/tmp/files/blackbox/blackbox.yml" "/etc/prometheus/blackbox.yml"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Cleanup files...                                                     -"
echo "------------------------------------------------------------------------"

sudo rm -rfv "/tmp/files"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
